# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contain the implementation of the project work of the course Introduction to Systems Biology at University of Helsinki. The project contains the analysis of the in-house KriSa pathway in the cell survival.

Below two models have been designed for the cell survival analysis of KriSa pathway.

* **ODEmodel.R** - ODE model
* **boolnetwork.R** - Boolean network model


### How do I get set up? ###

Require R software with package **pracma** installed.

### Who do I talk to? ###

You can provide your comments, suggestions to pradeep(dot)eranti(at)aalto(dot)fi.